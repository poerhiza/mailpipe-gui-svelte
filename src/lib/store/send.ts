import type {
	SendEmailMessage,
	EmailMessage,
	SendMessageAttachment,
	EmailAddress
} from '$lib/types/send';

import { writable } from 'svelte/store';

const sendEmailMessage: SendEmailMessage = {
	message: {
		to: [],
		cc: [],
		bcc: [],
		subject: '',
		content: '',
		attachment: []
	},
	body: '',
	messageHash: '',
	box: '',
	email_key: '',
	transport_key: ''
};

const resetStore = () => {
	sendEmailMessage.message.to = [];
	sendEmailMessage.message.cc = [];
	sendEmailMessage.message.bcc = [];
	sendEmailMessage.message.subject = '';
	sendEmailMessage.message.content = '';
	sendEmailMessage.message.attachment = [];
	sendEmailMessage.email_key = '';
	sendEmailMessage.transport_key = '';
	sendEmailMessage.body = '';
	sendEmailMessage.messageHash = '';
	sendEmailMessage.box = '';
};

export const sendService = (() => {
	const { subscribe, set, update } = writable(sendEmailMessage);

	return {
		subscribe,
		// @ts-ignore
		set: set,
		reset: () => {
			resetStore();
			set(sendEmailMessage);
		}
	};
})();
