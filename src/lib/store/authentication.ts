import { writable } from 'svelte/store';

import { md5, sha3 } from 'hash-wasm';

const userSession = {
	email: '',
	name: '',
	newpassword: '',
	password: '',
	tk: '',
	account_hash: '',
	response: {
		code: 0,
		message: ''
	},
	authenticated: false
};

const clearUserSession = () => {
	userSession.email = '';
	userSession.name = '';
	userSession.newpassword = '';
	userSession.password = '';
	userSession.tk = '';
	userSession.account_hash = '';
	userSession.response = {
		code: 0,
		message: ''
	};
	userSession.authenticated = false;
};

export const authenticationService = (() => {
	const { subscribe, set, update } = writable(userSession);

	return {
		set,
		subscribe,
		setHash: (email: string, name: string, account_hash: string) => {
			update(() => {
				userSession.name = name;
				userSession.account_hash = account_hash;
				userSession.email = email;
				return userSession;
			});
		},
		// @ts-ignore
		isAuthenticated: () => {
			return userSession.authenticated;
		},
		changePassword: async (currentpassword: string, newpassword: string) => {
			const res = await fetch('MAILPIPE_API_URL/api/v1/authentication/change', {
				method: 'POST',
				mode: 'cors',
				credentials: 'include',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				body: JSON.stringify({
					account_hash: userSession.account_hash,
					newpassword: await sha3(newpassword, 512),
					password: await sha3(currentpassword, 512)
				})
			});

			let message = await res.json();

			if (res.status != 200 || message.success != true) {
				console.log(message);
				return message;
			}

			return true;
		},
		isDefaultPasswordInUse: async () => {
			const defaultPassword = await sha3(userSession.email, 512);

			return userSession.password == defaultPassword ? defaultPassword : false;
		},
		authenticate: async (email: string, password: string) => {

			if (email.indexOf('@') == -1) {
				email = `${email}@${window.location.hostname}`;
			}

			email = email.toLocaleLowerCase();

			clearUserSession();

			userSession.authenticated = true;

			userSession.email = email;
			userSession.name = email.split('@')[0];
			userSession.password = await sha3(password, 512);
			userSession.tk = await md5(password + password); //TODO: why...
			userSession.account_hash = await sha3(email + userSession.password, 512); // eslint-disable-line

			const res = await fetch('MAILPIPE_API_URL/api/v1/authentication/login', {
				method: 'POST',
				mode: 'cors',
				credentials: 'include',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				body: JSON.stringify(userSession)
			});

			if (res.status != 200) {
				clearUserSession();
				userSession.authenticated = false;
			}

			userSession.response = await res.json();

			update(() => {
				return userSession;
			});

			return userSession;
		},
		session: () => {
			return userSession;
		},
		reset: () => {
			clearUserSession();
			set(userSession);
		}
	};
})();
