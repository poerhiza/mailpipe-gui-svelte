import { writable } from 'svelte/store';

const accounts = {
    links: {},
    requests: {},
    active: {
        name: '',
        email: '',
        unread: 0
    }
};

export const accountService = (() => {
    const { subscribe, set, update } = writable(accounts);

    return {
        subscribe,
        // @ts-ignore
        addLink: (key, data) => {
            if (accounts.links[key] === undefined) {
                update(() => {
                    accounts.links[key] = JSON.parse(JSON.stringify(data));
                    return accounts;
                });
            }
        },
        setActive: (account_email: string) => {
            if (accounts.active.email != account_email) {
                update(() => {
                    if (account_email in accounts.links) {
                        accounts.active = accounts.links[account_email];
                    }
                    return accounts;
                });
            }
        },
        getRequests: async (account_hash: string) => {
            accounts.requests = {};

            const res = await fetch('MAILPIPE_API_URL/api/v1/account/link/request/list', {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                credentials: 'include',
                body: JSON.stringify({ account_hash })
            });

            if (res.status != 200) {
                console.log('oh shat');
            }

            const response = await res.json();

            for (const [key, value] of Object.entries(response.requests)) {
                accounts.requests[key] = value;
            }

            update(() => {
                return accounts;
            });
        },
        getLinks: async (account_hash) => {
            accounts.links = {};

            const res = await fetch('MAILPIPE_API_URL/api/v1/account/link/get', {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                credentials: 'include',
                body: JSON.stringify({ account_hash })
            });

            if (res.status != 200) {
                accounts.links = {};
            }

            const response = await res.json();
            Object.keys(response.accounts).forEach((account_email) => {
                accounts.links[account_email] = response.accounts[account_email];
                accounts.links[account_email].email = account_email;
                accounts.links[account_email].name = account_email.split('@')[0];
            });

            update(() => {
                return accounts;
            });
        },
        requestAccountLink: async (account_hash: string, email: string) => {
            const res = await fetch('MAILPIPE_API_URL/api/v1/account/link/request/link', {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                credentials: 'include',
                body: JSON.stringify({ account_hash, email })
            });

            if (res.status != 200) {
                console.log('oh shat');
            }

            const response = await res.json();

            console.log(response);

            accounts.requests[email] = { email: 'pending' };

            update(() => {
                return accounts;
            });
        },
        removeAccountRequest: async (account_hash: string, email: string, perspective: string) => {
            const res = await fetch(`MAILPIPE_API_URL/api/v1/account/link/remove/${perspective}`, {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                credentials: 'include',
                body: JSON.stringify({
                    account_hash,
                    email
                })
            });

            const response = await res.json();

            if (res.status != 200) {
                console.log('oh snap...');
            } else {
                if (perspective == 'link') {
                    delete accounts.links[email];
                }

                if (perspective == 'access') {
                    delete accounts.requests[email];
                }
            }

            update(() => {
                return accounts;
            });
        },
        updateAccountRequest: async (account_hash: string, email: string, action: string) => {
            const res = await fetch(`MAILPIPE_API_URL/api/v1/account/link/reply/${action}`, {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                credentials: 'include',
                body: JSON.stringify({
                    account_hash,
                    email
                })
            });

            const response = await res.json();

            if (res.status != 200) {
                console.log('oh snap...');
            } else {
                accounts.requests[email] = action;
            }

            update(() => {
                return accounts;
            });
        },
        state: () => {
            return accounts;
        },
        reset: () => {
            set(accounts);
        }
    };
})();
