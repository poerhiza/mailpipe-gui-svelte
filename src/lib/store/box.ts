import type { Mailboxes } from '$lib/types/box';

import { writable } from 'svelte/store';

const boxes: Mailboxes = {
	boxes: {
		unread: [],
		read: []
	},
	subjects: []
};

const clearBoxes = () => {
	boxes.boxes = {
		unread: [],
		read: []
	};
	boxes.subjects = [];
};

export const boxService = (() => {
	const { subscribe, set, update } = writable(boxes);

	return {
		subscribe,
		// @ts-ignore
		message: async (account_hash: string, box: string, message_key: string) => {
			const res = await fetch(
				`MAILPIPE_API_URL/api/v1/account/view/box/${box}/message/${message_key}`,
				{
					method: 'POST',
					mode: 'cors',
					cache: 'force-cache',
					headers: {
						'Content-Type': 'application/json'
					},
					referrerPolicy: 'no-referrer',
					credentials: 'include',
					body: JSON.stringify({ account_hash })
				}
			);

			if (res.status != 200) {
				return [];
			} else {
				return await res.json();
			}
		},
		readMessage: async (email_hash: string, box: string, message_key: string) => {
			boxes.subjects.forEach((mailboxSubjects) => {
				if (mailboxSubjects.email_hash == email_hash) {
					mailboxSubjects.unread_subjects = mailboxSubjects.unread_subjects.filter((entry) => {
						if (entry.message_key == message_key) {
							mailboxSubjects.read_subjects.push(JSON.parse(JSON.stringify(entry)));
						}

						return entry.message_key != message_key;
					});
				}
			});

			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/view/box/${box}/message/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify({
					account_hash: email_hash,
					action: 'mark_read',
					messages: [{ id: message_key }]
				})
			});

			if (res.status == 200) {
				await res.json();
			}

			return update(() => {
				return boxes;
			});
		},
		deleteMessage: async (email_hash: string, box: string, message_key: string) => {
			boxes.subjects.forEach((mailboxSubjects) => {
				if (mailboxSubjects.email_hash == email_hash) {
					mailboxSubjects.read_subjects = mailboxSubjects.read_subjects.filter((entry) => {
						return entry.message_key != message_key;
					});
					mailboxSubjects.unread_subjects = mailboxSubjects.unread_subjects.filter((entry) => {
						return entry.message_key != message_key;
					});
				}
			});

			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/view/box/${box}/message/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify({
					account_hash: email_hash,
					action: 'purge',
					messages: [{ id: message_key }]
				})
			});

			if (res.status == 200) {
				await res.json();
			}

			return update(() => {
				return boxes;
			});
		},
		getLinkedBox: async (userSession, box: string) => {
			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/link/list/${box}/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify(userSession)
			});

			if (res.status != 200) {
				clearBoxes();
			} else {
				const response = await res.json();
				boxes.boxes[box] = response;
			}

			update(() => {
				return boxes;
			});
		},
		getBoxItems: async (
			account_hash: string,
			box: string,
			start_date_filter: string,
			end_date_filter: string
		) => {
			let request_body = {
				account_hash
			};

			if (start_date_filter !== undefined) {
				request_body.date_start = start_date_filter;
			}

			if (end_date_filter !== undefined) {
				request_body.date_end = end_date_filter;
			}

			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/view/box/${box}/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify(request_body)
			});

			if (res.status != 200) {
				clearBoxes();
			} else {
				const response = await res.json();

				boxes.boxes[box] = response;
			}

			update(() => {
				return boxes;
			});

			return boxes.boxes[box];
		},
		downloadAttachment: async (
			account_hash: string,
			box: string,
			message_key: string,
			attachment_key: string,
			filename: string
		) => {
			const res = await fetch(
				`MAILPIPE_API_URL/api/v1/account/view/box/${box}/message/${message_key}/download/attachment/${attachment_key}`,
				{
					method: 'POST',
					mode: 'cors',
					cache: 'force-cache',
					headers: {
						'Content-Type': 'application/json'
					},
					referrerPolicy: 'no-referrer',
					credentials: 'include',
					body: JSON.stringify({ account_hash })
				}
			);

			if (res.status != 200) {
				return []; // TODO : NOtify user download failed...
			} else {
				const blob = await res.blob();

				if (blob != null) {
					var url = window.URL.createObjectURL(blob);
					var a = document.createElement('a');
					a.href = url;
					a.download = filename;
					document.body.appendChild(a);
					a.click();
					a.remove();
				}
			}
		},
		reset: () => {
			clearBoxes();
			set(boxes);
		}
	};
})();
