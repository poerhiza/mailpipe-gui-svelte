import type { Transports, Transport } from '$lib/types/transport';

import { writable } from 'svelte/store';

let transports: Transports = [];

export const transportService = (() => {
	const { subscribe, set, update } = writable(transports);

	return {
		subscribe,
		get: async (account_hash: string) => {
			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/transport/get/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify({
					account_hash
				})
			});

			const response = await res.json();

			if (res.status != 200) {
				console.log('oh snap...');
			} else {
				transports.splice(0, transports.length);
				response.transports.forEach((transport: Transport) => {
					transports.push(transport);
				});
			}

			update(() => {
				return transports;
			});
		},
		add: async (account_hash: string, transport: Transport) => {
			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/transport/add/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify({
					account_hash,
					title: transport.title,
					data: transport.data
				})
			});

			if (res.status != 200) {
				console.log('oh snap...');
			} else {
				update(() => {
					transport.status = 'account';
					transports.push(transport);
					return transports;
				});
			}
		},
		remove: async (account_hash: string, title: string) => {
			const res = await fetch(`MAILPIPE_API_URL/api/v1/account/transport/delete/`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json'
				},
				referrerPolicy: 'no-referrer',
				credentials: 'include',
				body: JSON.stringify({
					account_hash,
					title: title
				})
			});

			if (res.status != 200) {
				console.log('oh snap...');
			} else {
				update(() => {
					return transports.filter((transport: Transport) => {
						return transport.title != title;
					});
				});
			}
		},
		state: () => {
			return transports;
		},
		reset: () => {
			set(transports);
		}
	};
})();
