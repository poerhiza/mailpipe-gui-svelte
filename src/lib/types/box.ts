export type Mailboxes = {
	boxes: {
		unread: [];
		read: [];
	};
	subjects: MailboxSubjects[];
};

export type MailboxSubjects = {
	email: string;
	email_hash: string;
	read_subjects: MailboxSubject[];
	unread_subjects: MailboxSubject[];
};

export type MailboxSubject = {
	message_key: string;
	subject: string;
	datetime: string;
	from: string;
};

export type MailboxMessage = {
	text: string;
	html: string;
	errors: string;
	attachments: MailboxMessageHeaders;
	headers: MailboxMessageHeaders;
};

export type MailboxMessageHeaders = MailboxMessageHeader[];

export type MailboxMessageHeader = {
	key: string;
	value: string;
};
