export type EmailAddress = {
	name: string;
	email: string;
};

export type SendMessageAttachment = {
	content: string;
	filename: string;
};

export type EmailMessage = {
	to: EmailAddress[];
	cc: EmailAddress[];
	bcc: EmailAddress[];
	subject: string;
	content: string;
	attachment: SendMessageAttachment[];
};

export type SendEmailMessage = {
	message: EmailMessage;
	email_key: string;
	transport_key: string;
	body: string;
	messageHash: string;
	box: string;
};
