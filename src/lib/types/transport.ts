export type Transports = Transport[];

export type Transport = {
	data: object;
	status: string;
	title: string;
	id: string;
};

export type TransportSimple = {
	host: string;
	port: Number;
	username: string;
	password: string;
	footer: string;
	type: string;
};

export type TransportSendgrid = {
	url: string;
	apikey: string;
	footer: string;
	type: string;
};
