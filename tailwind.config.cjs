import flowbitePlugin from 'flowbite/plugin'

const config = {
	content: [
		'./src/**/*.{html,js,svelte,ts}',
		'./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'
	],
	plugins: [flowbitePlugin],
	darkMode: 'class'
};

module.exports = config;
