import { sveltekit } from '@sveltejs/kit/vite';
import basicSsl from '@vitejs/plugin-basic-ssl';
import replace from '@rollup/plugin-replace';
import tailwindcss from '@tailwindcss/vite';

/** @type {import('vite').UserConfig} */
const config = {
	server: {
		https: true,
		proxy: {},
		open: '/'
	},
	plugins: [
		sveltekit(),
		basicSsl(),
		replace({
			MAILPIPE_API_URL: process.env.NODE_ENV == 'development' ? 'https://172.16.242.10:8443' : '',
			MAILPIPE_DEFAULT_USER: process.env.NODE_ENV == 'development' ? 'mailpipe@changeme.tld' : '',
			preventAssignment: false,
		}),
		tailwindcss(),
	],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	}
};

export default config;
