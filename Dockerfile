FROM node:22.13.1-bookworm

USER node

WORKDIR /app

COPY --chown=node:node . /app/

RUN npm i

CMD ["npm", "run", "dev"]
