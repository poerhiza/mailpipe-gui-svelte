# Starting the Development Environment

```bash
docker network create --attachable --subnet 172.16.242.0/24 mailpipe
docker-compose -f <docker-compose-file> <build/up/down>
```

- https://svelte.dev/
- https://kit.svelte.dev/
- https://mskocik.github.io/svelecte/#fetch
- https://tailwindcss.com/docs/responsive-design
- https://flowbite-svelte.com/docs/components/toast
- https://vitejs.dev/
- https://playwright.dev/
- https://www.typescriptlang.org/
- https://eslint.org/
