package mailpipeGUI

import (
	"embed"
)

//go:embed *
var gui embed.FS

func Dist() embed.FS {
	return gui
}
